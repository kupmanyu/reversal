#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

void prompt(char text[100]) {
    printf("> ");
    fgets(text, 100, stdin);
}

void reversal(char *text) {
    int x = strlen(text);
    text[x-1] = '\0';
    x = strlen(text) - 1;
    char temp;
    int y = x/2;
    for (int i = 0; i < y; i++) {
      //printf("%c -> ", text[i]);
      temp = text[i];
      text[i] = text[x-i];
      text[x-i] = temp;
      //printf("%c\n", text[i]);
    }
    if (x%2 == 1) {
      temp = text[y];
      text[y] = text[y+1];
      text[y+1] = temp;
    }
}

bool end(char *text) {
    if (text[0] == '.') return 1;
    else return 0;
}

int main(int n, char *args[n]) {
    char text[100];
    prompt(text);
    if (end(text) == 1) exit(0);
    while (! feof(stdin)) {
      if (end(text) == 1) exit(0);
      else {
        reversal(text);
        printf("%s\n", text);
        prompt(text);
      }
    }
    return 0;
}
